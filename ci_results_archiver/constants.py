# Copyright 2017 The Chromium OS Authors. All rights reserved.
# Use of this source code is governed by a BSD-style license that can be
# found in the LICENSE file.
"""Common constants."""

from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

# Base prefix of all metric names.
METRIC_BASE = 'chromeos/ci_results_archiver'
